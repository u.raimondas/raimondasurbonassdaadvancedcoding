
     enum Transmission{
        MANUAL,
        AUTOMATIC;
    }
     enum CarShapes{
        COUPE,
        SEDAN,
        WAGON;
    }
public class Cars extends LightVehicles {
         Enum<Transmission> transmission;
         Enum<CarShapes> carShape;

    public Cars(String model, String brand, int price, int topSpeed, String transmission, String carShapes) {
        super(model, brand, price, topSpeed);
        this.transmission = Transmission.valueOf(transmission.toUpperCase());
        this.carShape = CarShapes.valueOf(carShapes.toUpperCase());

    }

    @Override
    public String toString() {
        return  model + " " + brand +" "+ price + " Eur "
                + topSpeed + " km/h "+ transmission + " " +carShape + " ";
    }
}
