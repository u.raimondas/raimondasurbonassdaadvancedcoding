enum MotorShapes{
    ENDURO,
    CRUISER,
    CHOPPER;
}

public class Motorcycles extends LightVehicles {

    Enum<MotorShapes> motorShapesEnumShape;


    public Motorcycles(String model, String brand, int price, int topSpeed, String motorShapes) {
        super(model, brand, price, topSpeed);
        this.motorShapesEnumShape = MotorShapes.valueOf(motorShapes.toUpperCase());
    }


    @Override
    public String toString() {
        return "Motorcycles " +
                "topSpeed= " + topSpeed +
                ", model= " + model  +
                ", brand= " + brand  +
                ", price= " + price +  ", shape " + motorShapesEnumShape;
    }
}
