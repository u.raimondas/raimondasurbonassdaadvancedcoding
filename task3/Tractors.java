public class Tractors extends Vehicles {
    protected int maxPulledWeight;

    public Tractors(String model, String brand, int price, int maxPulledWeight) {
        super(model, brand, price);
        this.maxPulledWeight = maxPulledWeight;
    }

    public int getMaxPulledWeight() {
        return maxPulledWeight;
    }

    public void setMaxPulledWeight(int maxPulledWeight) {
        this.maxPulledWeight = maxPulledWeight;
    }

    @Override
    public String toString() {
        return "Tractors " +
                "maxPulledWeight= " + maxPulledWeight +
                ", model= " + model + '\'' +
                ", brand= " + brand + '\'' +
                ", price= " + price;
    }
}
