import java.io.Serializable;

public abstract class Vehicles implements Serializable {
    protected String model;
    protected String brand;
    protected int price;
    private static final long serialVersionUID = 1L;

    public Vehicles(String model, String brand, int price) {
        this.model = model;
        this.brand = brand;
        this.price = price;
    }



    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Vehicles{" +
                "model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                '}';
    }
}
