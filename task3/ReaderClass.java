
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReaderClass {
    public static void main(String[] args) {

        List<Cars> carList = new ArrayList<>();
        List<Motorcycles> motorcyclesList = new ArrayList<>();
        List<Tractors> tractorsList= new ArrayList<>();

        try(BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\komp\\Downloads\\vehicles.txt"))) {
            String str;


            List<String[]> listOfVehicles = new ArrayList<>();
            while ((str = in.readLine()) != null) {

                String[] tokens = str.split(", ");
                String vehicleType = tokens[0];

                switch (vehicleType.toLowerCase()) {
                    case "car":
                        String vehicleBrand = tokens[1];
                        String vehicleModel = tokens[2];
                        int price = Integer.parseInt(tokens[3]);
                        int topSpeed = Integer.parseInt(tokens[4]);
                        String transmission = tokens[5];
                        String shape = tokens[6];
                        carList.add(new Cars(vehicleBrand, vehicleModel, price, topSpeed, transmission, shape));
                        break;

                    case "motorcycle":
                        vehicleBrand = tokens[1];
                        vehicleModel = tokens[2];
                        price = Integer.parseInt(tokens[3]);
                        topSpeed = Integer.parseInt(tokens[4]);
                        shape = tokens[5];
                        motorcyclesList.add(new Motorcycles(vehicleBrand, vehicleModel, price, topSpeed, shape));
                        break;

                    case "tractor":
                        vehicleBrand = tokens[1];
                        vehicleModel = tokens[2];
                        try {
                            price = Integer.parseInt(tokens[3]);
                        } catch (NumberFormatException e) {
                            System.out.println("mistake");
                            price = 0;
                        }
                        int maxPulledWeight = Integer.parseInt(tokens[4]);
                        tractorsList.add(new Tractors(vehicleBrand, vehicleModel, price, maxPulledWeight));
                        break;
                    default:
                        System.out.println("Invalid line");
                }
            }



        }
        catch (
                IOException e) {
            System.out.println("File Read Error");
        }

        System.out.println("No. of cars = " +carList.size() +" ; " +
                "No. of bikes = "+ motorcyclesList.size() + " ; " + " No. of tractors " + tractorsList.size());
        List<Vehicles> allVehicles = Stream.of(carList,motorcyclesList,tractorsList)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        carList.stream().
                sorted(Comparator.comparing(c -> c.price)).
                forEach(System.out::println);
        motorcyclesList.stream().
                sorted(Comparator.comparing(motorcycles -> motorcycles.topSpeed)).
                forEach(System.out::println);

        try{

            FileOutputStream fos = new FileOutputStream(new File ("cars.txt"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for (Cars c: carList) {
                oos.writeUTF(c+"\n");
            }
            System.out.println("Done");
            oos.close();
            fos.close();

        } catch (IOException e){
            e.printStackTrace();
        }

    }


}
