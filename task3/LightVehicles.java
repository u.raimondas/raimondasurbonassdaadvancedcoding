public class LightVehicles extends Vehicles {

    protected int topSpeed;


    public LightVehicles(String model, String brand, int price, int topSpeed ) {
        super(model, brand, price);
        this.topSpeed=topSpeed;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }
}
