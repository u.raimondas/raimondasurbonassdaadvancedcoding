import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class SdaScheduler {

    public static void main(String[] args) throws Group.MaximumNumberOfStudentsReached {

        List<Person> students = new ArrayList<>();
        Person jonas=  new Student("Jonas", "Jonauskas", "2000", false);
        Person rokas= new Student("Rokas", "Rakauskas", "1978", true);
        Person mykolas= new Student("Mykolas", "Mykolaitis", "1979", false);
        Person jone= new Student("Jonė", "Jonauskaitė", "1987", false);
        Person romas= new Student("Romas", "Romutis", "1988", true);
        Person gerda= new Student("Gerda", "Gerdauskaitė", "1999", false);
        Person viktoras= new Student("Viktoras", "Viktoraitis", "1995", true);
        Person edvardas= new Student("Edvardas", "Edvardukas", "1994", false);
        Person kipras= new Student("Kipras", "Kiprijonas", "1997", false);
        Person nerijus= new Student("Nerijus", "Nerijauskas", "1998", false);
        Person kostas= new Student("Kostas", "Kostinas", "1980", false);
        Person ruta= new Student("Rūta", "Rūtelė", "1970", true);
        Person inga= new Student("Inga", "Ingutė", "2003", false);
        Person dima= new Student("Dima", "Dimitrov", "1991", false);
        Person jan= new Student("Jan", "Janevič", "1990", false);
        students.add(jonas);
        students.add(rokas);
        students.add(mykolas);
        students.add(jone);
        students.add(romas);
        students.add(gerda);
        students.add(viktoras);
        students.add(edvardas);
        students.add(kipras);
        students.add(nerijus);
        students.add(kostas);
        students.add(ruta);
        students.add(inga);
        students.add(dima);
        students.add(jan);


        List<Group> groups = new ArrayList<>();
        Group group1 = new Group("JavaVilnius6");
        Group group2 = new Group ("JavaRiga5");
        Group group3 = new Group ("JavaVilnius1");
        Group group4 = new Group ("JavaVilnius2");
        groups.add(group1);
        groups.add(group2);
        groups.add(group3);
        groups.add(group4);


        List<Person> trainer = new ArrayList<>();
        Person trainer1 = new Trainer("Trainer1","TrainerOne","1994",true);
        Person trainer2 = new Trainer("Trainer2","TrainerTwo","1992",true);
        Person trainer3 = new Trainer("Trainer3","TrainerThree","1995",true);
       // Person trainer4 = new Trainer("Trainer4","TrainerFour","1997",true);
        trainer.add(trainer1);
        trainer.add(trainer2);
        trainer.add(trainer3);

        group1.setTrainer(trainer1);
        group2.setTrainer(trainer2);
        group3.setTrainer(trainer3);

        group1.addStudents(jonas);
        group1.addStudents(kipras);
        group1.addStudents(gerda);

        group2.addStudents(edvardas);
        group2.addStudents(rokas);
        group2.addStudents(romas);

        group3.addStudents(jone);
        group3.addStudents(jan);

        group4.addStudents(inga);
        group4.addStudents(kostas);
        group4.addStudents(dima);
        group4.addStudents(dima);
        group4.addStudents(dima);




        //sorting by last name
        for (Group g:groups){
            g.getStudents().sort(Comparator.comparing(person -> person.getLastName()));
        }

        for (Group g:groups){
            System.out.println(g);
        }

        System.out.println("<<<<<<<<<>>>>>>>>>>>>>");
        //sorting by birth date


        for (Group g:groups){
            g.getStudents().sort(Comparator.comparing(person -> person.getDateOfBirth()));
        }
        for (Group g:groups){
            System.out.println(g);
        }


        }



    }

