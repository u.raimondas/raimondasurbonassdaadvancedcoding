public class Person {
    private String firstName;
    private String lastName;
    private String dateOfBirth;

    public Person(String firstName, String lastName, String dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }
    @Override
    public String toString(){
        return firstName + " " + lastName + " " + dateOfBirth;
    }



}

class Trainer extends Person{
    private boolean isAuthorized;

    public Trainer(String firstName, String lastName, String dateOfBirth, boolean isAuthorized) {
        super(firstName, lastName, dateOfBirth);
        this.isAuthorized = isAuthorized;
    }
}

class Student extends Person{
    private boolean hasPreviousJavaKnowledge;

    public Student(String firstName, String lastName, String dateOfBirth, boolean hasPreviousJavaKnowledge) {
        super(firstName, lastName, dateOfBirth);
        this.hasPreviousJavaKnowledge = hasPreviousJavaKnowledge;
    }


}
